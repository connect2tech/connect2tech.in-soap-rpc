package com.c2t;

import javax.jws.WebService;

@WebService(endpointInterface="com.c2t.HelloWorld")
public class HelloWorldImpl implements HelloWorld{
	public String getHelloWorld(String message) {
		return "hello " + message;
	}

}
