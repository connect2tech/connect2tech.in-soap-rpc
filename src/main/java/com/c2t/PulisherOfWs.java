package com.c2t;

import javax.xml.ws.Endpoint;

public class PulisherOfWs {
	public static void main(String[] args) {
		System.out.println("Publishing...");
		Endpoint.publish("http://localhost:8585/ws/hello", new HelloWorldImpl());
	}
}
