package com.c2t;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

public class Client {
	public static void main(String[] args) throws Exception{
		URL url = new URL("http://localhost:8585/ws/hello?wsdl");
		QName qname = new QName("http://c2t.com/", "HelloWorldImplService");
		Service service = Service.create(url, qname);
		HelloWorld hw = service.getPort(HelloWorld.class);
		String msg = hw.getHelloWorld("This is SOAP WS");
		System.out.println(msg);
		
	}
}
