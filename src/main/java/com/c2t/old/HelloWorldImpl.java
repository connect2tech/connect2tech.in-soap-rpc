package com.c2t.old;

import javax.jws.WebService;

@WebService(endpointInterface="com.c2t.HelloWorld")
public class HelloWorldImpl implements HelloWorld{

	public String getHelloMessage(String name) {
		return "Hello " + name;
	}
}
