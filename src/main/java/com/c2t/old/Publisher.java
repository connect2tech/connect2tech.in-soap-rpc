package com.c2t.old;

import javax.xml.ws.Endpoint;

public class Publisher {
	public static void main(String[] args) {
		System.out.println("Publishing the WS");
		Endpoint.publish("http://localhost:7779/ws/hello", new HelloWorldImpl());
	}
}
