package com.c2t.ws.soap1;

import javax.xml.ws.Endpoint;

public class SOAPPublisher {
	public static void main(String[] args) {
		Endpoint.publish("http://localhost:8889/ws/person", new PersonServiceImpl());
		System.out.println("Services Up and Running...");
	}
}