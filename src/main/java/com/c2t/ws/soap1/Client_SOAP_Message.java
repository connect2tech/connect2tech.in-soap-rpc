package com.c2t.ws.soap1;

import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

public class Client_SOAP_Message {
	public static void main(String[] args) throws Exception {

		System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
		System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
		System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
		System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");

		URL url = new URL("http://localhost:8889/ws/person?wsdl");
		QName qname = new QName("http://soap1.ws.c2t.com/", "PersonServiceImplService");
		Service service = Service.create(url, qname);
		PersonService hello = service.getPort(PersonService.class);
		Person[] p = hello.getAllPersons();

	}
}
//comment here..