package com.c2t.ws.edureka;

import javax.xml.ws.Endpoint;

public class PublisherOfWs {
	public static void main(String[] args) {
		Endpoint.publish("http://localhost:8899/ws/person", new PersonServiceImpl());
		System.out.println("Published.....");
	}
}
