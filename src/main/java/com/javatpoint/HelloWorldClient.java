package com.javatpoint;

import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;

public class HelloWorldClient {
	public static void main(String[] args) throws Exception {

		System.setProperty("com.sun.xml.ws.transport.http.client.HttpTransportPipe.dump", "true");
		System.setProperty("com.sun.xml.internal.ws.transport.http.client.HttpTransportPipe.dump", "true");
		System.setProperty("com.sun.xml.ws.transport.http.HttpAdapter.dump", "true");
		System.setProperty("com.sun.xml.internal.ws.transport.http.HttpAdapter.dump", "true");

		URL url = new URL("http://localhost:7779/ws/hello?wsdl");
		QName qname = new QName("http://javatpoint.com/", "HelloWorldImplService");
		Service service = Service.create(url, qname);
		HelloWorld hello = service.getPort(HelloWorld.class);
		String s = hello.getHelloWorldAsString("This is SOAP");
		System.out.println(s);

		/*
		 * URL url = new URL("http://localhost:7779/ws/hello?wsdl");
		 * 
		 * // 1st argument service URI, refer to wsdl document above // 2nd
		 * argument is service name, refer to wsdl document above //
		 * com.javatpoint
		 * 
		 * //QName represents a qualified name as defined in the XML
		 * specifications QName qname = new QName("http://javatpoint.com/",
		 * "HelloWorldImplService");
		 * 
		 * //Service objects provide the client view of a Web service Service
		 * service = Service.create(url, qname);
		 * 
		 * //https://docs.oracle.com/javaee/6/api/javax/xml/ws/Service.html#
		 * getPort(java.lang.Class) HelloWorld hello =
		 * service.getPort(HelloWorld.class);
		 * System.out.println(hello.getHelloWorldAsString("javatpoint rpc"));
		 */
	}
}