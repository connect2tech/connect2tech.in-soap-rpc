package com.javatpoint;

import javax.xml.ws.Endpoint;

//public static Endpoint publish(String address, Object implementor)
//Creates and publishes an endpoint for the specified implementor object at the given address.

//address - A URI specifying the address and transport/protocol to use. 
//A http: URI MUST result in the SOAP 1.1/HTTP binding being used. Implementations may support other URI schemes.
//implementor - The endpoint implementor.

//Endpoint publisher  
public class HelloWorldPublisher {
	public static void main(String[] args) {
		System.out.println("Publishing SOAP webservice...");
		Endpoint.publish("http://localhost:7779/ws/hello", new HelloWorldImpl());
		//wsdl: http://localhost:7779/ws/hello?wsdl
	}
}