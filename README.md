connect2tech.in-soap-rpc
============================

This is a sample project used for the Parleys WebDriver online courses. It contains starting points and solutions for the exercises in this course.


# The largest heading

## The second largest heading

###### The smallest heading

**This is bold text**

*This text is italicized*

~~This was mistaken text~~

**This text is _extremely_ important**

In the words of Abraham Lincoln:

> Pardon my French

Use `git status` to list all new or modified files that haven't yet been committed.

Some basic Git commands are:
```
git status
git add
git commit
```

This site was built using [GitHub Pages](https://pages.github.com/)

- George Washington
- John Adams
- Thomas Jefferson

1. James Madison
2. James Monroe
3. John Quincy Adams

- [x] Finish my changes
- [ ] Push my commits to GitHub
- [ ] Open a pull request


## SOAP API

1. Publish the API: HelloWorldPublisher
2. Access WSDL: http://localhost:7779/ws/hello?wsdl

  
### Understanding WSDL
  
https://www.w3schools.com/xml/xml_wsdl.asp

### Executing Client
  	
1. HelloWorldClient

## URLs
1. https://www.javatpoint.com/what-is-web-service
2. https://www.javatpoint.com/web-service-components

